<?php
if (!defined('IN_FINECMS')) exit('No permission resources');

return array(
	'key' => 9,
    'name'			=> '在线充值',
    'author'		=> 'dayrui',
    'version'		=> '2.0',
    'typeid'		=> 1,
    'fields'		=> array(),
    'description'	=> '支持生成虚拟充值卡',
);